var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

var mongoose = require('mongoose');
var Contact = mongoose.model('Contact');

router.post('/contacts', function(req, res, next){
   var contact = new Contact(req.body);
   contact.save(function(err, post){
      if(err){
         return next(err);
      }
      res.json(contact);
   });
});

module.exports = router;
