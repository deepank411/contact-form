var app = angular.module('contactForm', []);

app.service('contactService', function($http){
   var _contacts = [];
   this.contacts = _contacts;

   this.create = function(contact){
      return $http.post('/contacts', contact).success(function(data){
         _contacts.push(data);
      });
   };
});

app.controller('MainCtrl', ['$scope', 'contactService',
   function($scope, contactService){

      $scope.contacts = contactService.contacts;
      $scope.addContact = function(){
         if(!$scope.name){
            return;
         }
         contactService.create({
            name: $scope.name,
            email: $scope.email,
            message: $scope.message
         });
         $scope.name = '';
         $scope.email = '';
         $scope.message = '';
      };
   }
]);
